use tauri::async_runtime::RwLock;
use up_bank_api::{restson, RestClient};

pub struct ClientStruct(pub RwLock<Option<RestClient>>);

impl ClientStruct {
    pub async fn request<P, D: restson::RestPath<P> + serde::de::DeserializeOwned>(
        &self,
        params: P,
    ) -> Result<D, String> {
        let client_lock = self.0.read().await;
        let client = client_lock
            .as_ref()
            .ok_or(String::from("No client available"))?;

        Ok(client
            .get::<P, D>(params)
            .await
            .map_err(|err| err.to_string())?
            .into_inner())
    }

    pub async fn is_some(&self) -> bool {
        self.0.read().await.is_some()
    }
}
