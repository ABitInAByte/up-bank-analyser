#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

mod client_controller;

use tauri::{async_runtime::RwLock, State};
use up_bank_api::{
    accounts::{AccountData, AccountId, AccountResponse, AccountsListResponse},
    get_new_client,
    transactions::{TransactionData, TransactionListResponse},
    utility::Ping,
};

use client_controller::ClientStruct;

#[tauri::command]
async fn have_client(client_state: State<'_, ClientStruct>) -> Result<bool, String> {
    Ok(client_state.is_some().await)
}

#[tauri::command]
async fn get_accounts(client: State<'_, ClientStruct>) -> Result<Vec<AccountData>, String> {
    Ok(client.request::<(), AccountsListResponse>(()).await?.data)
}

#[tauri::command]
async fn get_account(
    id: String,
    req_id: i32,
    client: State<'_, ClientStruct>,
) -> Result<AccountData, String> {
    let account_id = AccountId { id };
    let res = Ok(client
        .request::<AccountId, AccountResponse>(account_id)
        .await?
        .data);

    println!("Got result {req_id}");

    res
}

#[tauri::command]
async fn get_account_transactions(
    account_id: String,
    client: State<'_, ClientStruct>,
) -> Result<Vec<TransactionData>, String> {
    let account = AccountId { id: account_id };

    let res = client
        .request::<&AccountId, TransactionListResponse>(&account)
        .await?;

    Ok(res.data)
}

#[tauri::command]
async fn get_all_transactions(
    client: State<'_, ClientStruct>,
) -> Result<Vec<TransactionData>, String> {
    let res = client.request::<(), TransactionListResponse>(()).await?;

    Ok(res.data)
}

#[tauri::command]
async fn receive_api_key(
    token: String,
    client_state: State<'_, ClientStruct>,
) -> Result<(), String> {
    let client = get_new_client(token).map_err(|op| op.to_string())?;

    let pong = client.get::<(), Ping>(()).await.map_err(|err| {
        println!(
            "Failed to get successful response from server. Err: {}",
            err.to_string()
        );
        String::from("Error response from server")
    })?;

    println!("Got response! {}", pong.status());

    *client_state.0.write().await = Some(client);

    Ok(())
}

fn main() {
    let init_client: ClientStruct = ClientStruct(RwLock::new(None));

    tauri::Builder::default()
        .manage(init_client)
        .invoke_handler(tauri::generate_handler![
            have_client,
            receive_api_key,
            get_accounts,
            get_account,
            get_account_transactions,
            get_all_transactions
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
