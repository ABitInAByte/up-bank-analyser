import { invoke } from "@tauri-apps/api";
import { AccountDataDto } from ".";

export class AccountData {
  private _dateCreated;

  constructor(private data: AccountDataDto) {
    this._dateCreated = new Date(this.data.attributes.created_at);
  }

  get id() {
    return this.data.id.id;
  }

  get balance() {
    return parseFloat(this.data.attributes.balance.value);
  }

  get dateCreated() {
    return this._dateCreated;
  }

  get name() {
    return this.data.attributes.display_name;
  }

  get type() {
    return this.data.attributes.account_type;
  }

  get currencyCode() {
    return this.data.attributes.balance.currency_code;
  }

  async getTransactions() {
    return invoke("get_account_transactions", { accountId: this.data.id.id });
  }
}
