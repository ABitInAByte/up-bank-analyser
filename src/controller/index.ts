import { invoke } from "@tauri-apps/api";
import { AccountData } from "./class";

export type AccountType = "TRANSACTIONAL" | "SAVER";
export type CurrentCode = "AUD";

export type AccountDataDto = {
  attributes: {
    account_type: AccountType;
    balance: {
      currency_code: CurrentCode;
      value: string;
      value_in_base_units: number;
    };
    created_at: string;
    display_name: string;
  };
  id: {
    id: string;
  };
  relationships: {
    transactions: {
      links: {
        related: string;
      };
    };
  };
};

export async function haveClient(): Promise<boolean> {
  return await invoke("have_client");
}

export async function recieveApiKey(token: string) {
  await invoke("receive_api_key", { token });
}

export async function getAccounts(): Promise<Array<AccountData>> {
  const res = (await invoke("get_accounts")) as Array<AccountDataDto>;
  return res.map((acc) => new AccountData(acc));
}

export async function getAccount(id: string): Promise<AccountData> {
  const res = (await invoke("get_account", {
    id,
    reqId: 0,
  })) as AccountDataDto;
  return new AccountData(res);
}
