import { useParams } from "@solidjs/router";
import { Component, Show, onMount } from "solid-js";
import { State } from "../helper/signal";
import { AccountData } from "../controller/class";
import { getAccount } from "../controller";

export const Account: Component = () => {

  const accountId = useParams().id as string;

  const account = new State<AccountData | null>(null);
  const transactions = new State(new Array());

  onMount(() => {
    getAccount(accountId).then((res) => {
      account.state = res;
      res.getTransactions().then((res) => {
        console.log(res)
        // transactions.state = res;
      })
    })
  })

  return (
    <div>
      <Show when={account.state !== null} fallback={<div>loading...</div>}>
        <div class="flex justify-center">
          <h3>{account.state?.name} - ${account.state?.balance.toFixed(2)}</h3>
        </div>
      </Show>
    </div>
  )
}




