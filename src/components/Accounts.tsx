import { Component, For, onMount } from "solid-js";
import { State } from "../helper/signal";
import { AccountType, getAccounts } from "../controller";
import { AccountData } from "../controller/class";
import { useNavigate } from "@solidjs/router";

const accountTypeMap = new Map<AccountType, number>();
accountTypeMap.set("TRANSACTIONAL", 0);
accountTypeMap.set("SAVER", 1);

export const Accounts: Component = () => {

  const navigate = useNavigate();
  const accounts = new State(new Array<AccountData>());

  onMount(() => {
    getAccounts().then(res => {
      res.sort((a, b) => b.balance - a.balance).sort((a, b) => (accountTypeMap.get(a.type) ?? 0) - (accountTypeMap.get(b.type) ?? 0));
      accounts.state = res;
    })
  })

  return (
    <div class="flex flex-col justify-center w-98">
      <h2 class="text-center text-lg font-bold">Accounts</h2>
      <div class="flex flex-col gap-y-2">
        <For each={accounts.state}>
          {
            (account) =>
              <div class="flex flex-row justify-between">
                <div class="btn btn-sm flex w-full justify-between" onClick={() => navigate(`/account/${account.id}`)}>
                  <span>{`${account.name}`}</span>
                  <span class="">{`$${account.balance.toFixed(2)}`}</span>
                </div>
              </div>
          }
        </For>
      </div>
    </div>
  )
}

